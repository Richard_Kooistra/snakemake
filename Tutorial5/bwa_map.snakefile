rule bwa_map:
    input:
        genome = "data/genome.fa",
        fasta = "data/samples/{sample}.fastq"
    output:
        "mapped_reads/{sample}.bam"
    benchmark:
        "benchmarks/{sample}.bwa.benchmark.txt"
    message:
        """executing bwa mem on the following \n{input} to generate the following {output}"""
    shell:
        "bwa mem {input.genome} {input.fasta}| samtools view -Sb - > {output} "
