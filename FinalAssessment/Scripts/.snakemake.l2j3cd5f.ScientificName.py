
######## Snakemake header ########
import sys; sys.path.append("/homes/rkooistra/Desktop/SNAKEmake/Venv/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\t\x00\x00\x00resourcesq\x03csnakemake.io\nResources\nq\x04)\x81q\x05(K\x01K\x01e}q\x06(X\x06\x00\x00\x00_namesq\x07}q\x08(X\x06\x00\x00\x00_coresq\tK\x00N\x86q\nX\x06\x00\x00\x00_nodesq\x0bK\x01N\x86q\x0cuh\tK\x01h\x0bK\x01ubX\x06\x00\x00\x00configq\r}q\x0e(X\x11\x00\x00\x00microglia_samplesq\x0f}q\x10(X\x12\x00\x00\x00S1210100TAGCTTL007q\x11X\x12\x00\x00\x00S1210100TAGCTTL007q\x12X\x10\x00\x00\x00S13067GTGGCCL007q\x13X\x10\x00\x00\x00S13067GTGGCCL007q\x14X\x10\x00\x00\x00S12112GATCAGL007q\x15X\x10\x00\x00\x00S12112GATCAGL007q\x16X\x10\x00\x00\x00S12082ACTTGAL007q\x17X\x10\x00\x00\x00S12082ACTTGAL007q\x18X\x10\x00\x00\x00S14005GGCTACL007q\x19X\x10\x00\x00\x00S14005GGCTACL007q\x1aX\x10\x00\x00\x00S12048ATCACGL007q\x1bX\x10\x00\x00\x00S12048ATCACGL007q\x1cX\x10\x00\x00\x00S15018GTTTCGL007q\x1dX\x10\x00\x00\x00S15018GTTTCGL007q\x1eX\x10\x00\x00\x00S12067TTAGGCL007q\x1fX\x10\x00\x00\x00S12067TTAGGCL007q uX\r\x00\x00\x00brain_samplesq!}q"(X\x05\x00\x00\x00SPM26q#X\x05\x00\x00\x00SPM26q$X\x05\x00\x00\x00SPM13q%X\x05\x00\x00\x00SPM13q&X\x05\x00\x00\x00SPM11q\'X\x05\x00\x00\x00SPM11q(X\x05\x00\x00\x00SPM30q)X\x05\x00\x00\x00SPM30q*X\x05\x00\x00\x00SPM25q+X\x05\x00\x00\x00SPM25q,X\x05\x00\x00\x00SPM35q-X\x05\x00\x00\x00SPM35q.X\x04\x00\x00\x00SPM2q/X\x04\x00\x00\x00SPM2q0X\x05\x00\x00\x00SPM15q1X\x05\x00\x00\x00SPM15q2X\x05\x00\x00\x00SPM14q3X\x05\x00\x00\x00SPM14q4X\x05\x00\x00\x00SPM36q5X\x05\x00\x00\x00SPM36q6X\x04\x00\x00\x00SPM9q7X\x04\x00\x00\x00SPM9q8X\x04\x00\x00\x00SPM8q9X\x04\x00\x00\x00SPM8q:X\x04\x00\x00\x00SPM5q;X\x04\x00\x00\x00SPM5q<X\x04\x00\x00\x00SPM6q=X\x04\x00\x00\x00SPM6q>X\x05\x00\x00\x00SPM33q?X\x05\x00\x00\x00SPM33q@X\x05\x00\x00\x00SPM28qAX\x05\x00\x00\x00SPM28qBuuX\x05\x00\x00\x00inputqCcsnakemake.io\nInputFiles\nqD)\x81qE(X\x10\x00\x00\x00all_bacteria.fnaqFX!\x00\x00\x00Data/Accession/Bacteria/SPM30.txtqGe}qH(h\x07}qIX\x05\x00\x00\x00fastaqJK\x01N\x86qKshJhGubX\x06\x00\x00\x00paramsqLcsnakemake.io\nParams\nqM)\x81qN}qOh\x07}qPsbX\x04\x00\x00\x00ruleqQX&\x00\x00\x00ConvertAccessionNumberToScientificNameqRX\x06\x00\x00\x00outputqScsnakemake.io\nOutputFiles\nqT)\x81qUX\'\x00\x00\x00Data/ScientificNames/Bacteria/SPM30.txtqVa}qWh\x07}qXsbX\t\x00\x00\x00wildcardsqYcsnakemake.io\nWildcards\nqZ)\x81q[X\x05\x00\x00\x00SPM30q\\a}q](h\x07}q^X\x05\x00\x00\x00fastqq_K\x00N\x86q`sh_h\\ubX\x03\x00\x00\x00logqacsnakemake.io\nLog\nqb)\x81qcX\'\x00\x00\x00Logs/ScientificNames/Bacteria/SPM30.logqda}qeh\x07}qfsbX\x07\x00\x00\x00threadsqgK\x01ub.'); from snakemake.logging import logger; logger.printshellcmds = False
######## Original script #########
#!/usr/bin/env python3

with open(snakemake.output[0], "w") as out:
    AllGenomes = open(snakemake.input[0])
    genomedict = {}
    for line in AllGenomes:
        if line.startswith(">gi"):
            genome = line.split(">")[1].split(",")[0]
            refname = genome.split("| ")[0]
            organism = genome.split("| ")[1]
            print(refname)
            genomedict[refname] = organism

        elif line.startswith(">JPKZ") or line.startswith(">MIEF") or line.startswith(">LL") or line.startswith(">AWXF") or line.startswith("EQ") or line.startswith(">NW_") or line.startswith(">LWMK") or line.startswith(">NZ_") or line.startswith(">NC_") or line.startswith(">KT"):
            genome = line.split(">")[1].split(",")[0]
            refname = genome.split(" ")[0]
            organismName = genome.split(" ")[1:]
            organism = ' '.join(organismName)
            print(refname)
            genomedict[refname] = organism

    accession_nrs = []
    pathogenNames = []

    for line in open(snakemake.input[1]):
        accession_nrs.append(line.split("\t")[0])

    for number in accession_nrs:
        if number in genomedict:
            pathogenNames.append(genomedict[number])

    for i in pathogenNames:
        out.write(number + "\n")
