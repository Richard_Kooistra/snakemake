#!/usr/bin/env python3

with open(snakemake.output[0], "w") as out:
    AllGenomes = open(snakemake.input[0])
    genomedict = {}
    for line in AllGenomes:
        if line.startswith(">gi"):
            genome = line.split(">")[1].split(",")[0]
            refname = genome.split("| ")[0]
            organism = genome.split("| ")[1]
            genomedict[refname] = organism

        elif line.startswith(">JPKZ") or line.startswith(">MIEF") or line.startswith(">LL") or line.startswith(">AWXF") or line.startswith("EQ") or line.startswith(">NW_") or line.startswith(">LWMK") or line.startswith(">NZ_") or line.startswith(">NC_") or line.startswith(">KT"):
            genome = line.split(">")[1].split(",")[0]
            refname = genome.split(" ")[0]
            organismName = genome.split(" ")[1:]
            organism = ' '.join(organismName)
            genomedict[refname] = organism    

    accession_nrs = []
    pathogenNames = []

    for line in open(snakemake.input[1]):
        accession_nrs.append(line.split("\t")[0])

    for number in accession_nrs:
        if number in genomedict:
            pathogenNames.append(genomedict[number])

    for i in pathogenNames:
        out.write(number + "\n")
