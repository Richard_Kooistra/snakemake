
######## Snakemake header ########
import sys; sys.path.append("/homes/rkooistra/Desktop/SNAKEmake/Venv/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x06\x00\x00\x00configq\x03}q\x04(X\x11\x00\x00\x00microglia_samplesq\x05}q\x06(X\x12\x00\x00\x00S1210100TAGCTTL007q\x07X\x12\x00\x00\x00S1210100TAGCTTL007q\x08X\x10\x00\x00\x00S14005GGCTACL007q\tX\x10\x00\x00\x00S14005GGCTACL007q\nX\x10\x00\x00\x00S15018GTTTCGL007q\x0bX\x10\x00\x00\x00S15018GTTTCGL007q\x0cX\x10\x00\x00\x00S12067TTAGGCL007q\rX\x10\x00\x00\x00S12067TTAGGCL007q\x0eX\x10\x00\x00\x00S12112GATCAGL007q\x0fX\x10\x00\x00\x00S12112GATCAGL007q\x10X\x10\x00\x00\x00S12048ATCACGL007q\x11X\x10\x00\x00\x00S12048ATCACGL007q\x12X\x10\x00\x00\x00S13067GTGGCCL007q\x13X\x10\x00\x00\x00S13067GTGGCCL007q\x14X\x10\x00\x00\x00S12082ACTTGAL007q\x15X\x10\x00\x00\x00S12082ACTTGAL007q\x16uX\r\x00\x00\x00brain_samplesq\x17}q\x18(X\x04\x00\x00\x00SPM9q\x19X\x04\x00\x00\x00SPM9q\x1aX\x05\x00\x00\x00SPM25q\x1bX\x05\x00\x00\x00SPM25q\x1cX\x05\x00\x00\x00SPM11q\x1dX\x05\x00\x00\x00SPM11q\x1eX\x04\x00\x00\x00SPM8q\x1fX\x04\x00\x00\x00SPM8q X\x05\x00\x00\x00SPM14q!X\x05\x00\x00\x00SPM14q"X\x05\x00\x00\x00SPM13q#X\x05\x00\x00\x00SPM13q$X\x05\x00\x00\x00SPM15q%X\x05\x00\x00\x00SPM15q&X\x05\x00\x00\x00SPM26q\'X\x05\x00\x00\x00SPM26q(X\x05\x00\x00\x00SPM30q)X\x05\x00\x00\x00SPM30q*X\x04\x00\x00\x00SPM6q+X\x04\x00\x00\x00SPM6q,X\x04\x00\x00\x00SPM5q-X\x04\x00\x00\x00SPM5q.X\x04\x00\x00\x00SPM2q/X\x04\x00\x00\x00SPM2q0X\x05\x00\x00\x00SPM28q1X\x05\x00\x00\x00SPM28q2X\x05\x00\x00\x00SPM35q3X\x05\x00\x00\x00SPM35q4X\x05\x00\x00\x00SPM36q5X\x05\x00\x00\x00SPM36q6X\x05\x00\x00\x00SPM33q7X\x05\x00\x00\x00SPM33q8uuX\x06\x00\x00\x00paramsq9csnakemake.io\nParams\nq:)\x81q;}q<X\x06\x00\x00\x00_namesq=}q>sbX\x06\x00\x00\x00outputq?csnakemake.io\nOutputFiles\nq@)\x81qAX\'\x00\x00\x00Data/ScientificNames/Bacteria/SPM13.txtqBa}qCh=}qDsbX\t\x00\x00\x00wildcardsqEcsnakemake.io\nWildcards\nqF)\x81qGX\x05\x00\x00\x00SPM13qHa}qI(h=}qJX\x05\x00\x00\x00fastqqKK\x00N\x86qLshKhHubX\x04\x00\x00\x00ruleqMX&\x00\x00\x00ConvertAccessionNumberToScientificNameqNX\t\x00\x00\x00resourcesqOcsnakemake.io\nResources\nqP)\x81qQ(K\x01K\x01e}qR(h=}qS(X\x06\x00\x00\x00_nodesqTK\x00N\x86qUX\x06\x00\x00\x00_coresqVK\x01N\x86qWuhTK\x01hVK\x01ubX\x03\x00\x00\x00logqXcsnakemake.io\nLog\nqY)\x81qZX\'\x00\x00\x00Logs/ScientificNames/Bacteria/SPM13.logq[a}q\\h=}q]sbX\x05\x00\x00\x00inputq^csnakemake.io\nInputFiles\nq_)\x81q`(X\x10\x00\x00\x00all_bacteria.fnaqaX!\x00\x00\x00Data/Accession/Bacteria/SPM13.txtqbe}qc(h=}qdX\x05\x00\x00\x00fastaqeK\x01N\x86qfshehbubX\x07\x00\x00\x00threadsqgK\x01ub.'); from snakemake.logging import logger; logger.printshellcmds = False
######## Original script #########
#!/usr/bin/env python3

with open(snakemake.output[0], "w") as out:
    AllGenomes = open(snakemake.input[0])
    genomedict = {}
    for line in AllGenomes:
        if line.startswith(">gi"):
            genome = line.split(">")[1].split(",")[0]
            refname = genome.split("| ")[0]
            organism = genome.split("| ")[1]
            print(refname)
            genomedict[refname] = organism

        elif line.startswith(">JPKZ") or line.startswith(">MIEF") or line.startswith(">LL") or line.startswith(">AWXF") or line.startswith("EQ") or line.startswith(">NW_") or line.startswith(">LWMK") or line.startswith(">NZ_") or line.startswith(">NC_") or line.startswith(">KT"):
            genome = line.split(">")[1].split(",")[0]
            refname = genome.split(" ")[0]
            organismName = genome.split(" ")[1:]
            organism = ' '.join(organismName)
            print(refname)
            genomedict[refname] = organism

    accession_nrs = []
    pathogenNames = []

    for line in open(snakemake.input[1]):
        accession_nrs.append(line.split("\t")[0])

    for number in accession_nrs:
        if number in genomedict:
            pathogenNames.append(genomedict[number])

    for i in pathogenNames:
        out.write(number + "\n")
