#!/usr/bin/python3


import os
import sys
import glob


brain = "/data/storage/dataprocessing/rnaseq_data/Brazil_Brain/"
micro = "/data/storage/dataprocessing/rnaseq_data/Brazil_Microglia/"
dutch = "/data/storage/dataprocessing/rnaseq_data/Dutch_Microglia/"

def configfile():
    with open("config.yaml", 'w') as conf:

        fastafiles = glob.glob(brain + "*_R1.fastq")
        conf.write("brain_samples:" + '\n')
        for fasta in sorted(fastafiles):
            brains = os.path.basename(fasta).strip("R1.fastq").replace("_", "")
            conf.write('\t' + brains + " : " + brains + '\n')
        conf.write('\n')

        fastafiles = glob.glob(micro + "*_R1.fastq")
        conf.write("microglia_samples:" + '\n')
        for fasta in sorted(fastafiles):
            micros = os.path.basename(fasta).strip("R1.fastq").replace("_", "")
            conf.write('\t' + micros + " : " + micros + '\n')
        conf.write('\n')

        fastafiles = glob.glob(dutch + "*_R1.fastq")
        conf.write("microglia_samples:" + '\n')
        for fasta in sorted(fastafiles):
            dutchy = os.path.basename(fasta).strip("R1.fastq").replace("_", "")
            conf.write('\t' + dutchy + " : " + dutchy + '\n')
        conf.write('\n')

def main():
    configfile()

if __name__ == '__main__':
    main()
