
######## Snakemake header ########
import sys; sys.path.append("/homes/rkooistra/Desktop/SNAKEmake/Venv/lib/python3.5/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x03\x00\x00\x00logq\x03csnakemake.io\nLog\nq\x04)\x81q\x05X\'\x00\x00\x00Logs/ScientificNames/Bacteria/SPM25.logq\x06a}q\x07X\x06\x00\x00\x00_namesq\x08}q\tsbX\x04\x00\x00\x00ruleq\nX&\x00\x00\x00ConvertAccessionNumberToScientificNameq\x0bX\x05\x00\x00\x00inputq\x0ccsnakemake.io\nInputFiles\nq\r)\x81q\x0e(X\x10\x00\x00\x00all_bacteria.fnaq\x0fX!\x00\x00\x00Data/Accession/Bacteria/SPM25.txtq\x10e}q\x11(X\x05\x00\x00\x00fastaq\x12h\x10h\x08}q\x13h\x12K\x01N\x86q\x14subX\t\x00\x00\x00wildcardsq\x15csnakemake.io\nWildcards\nq\x16)\x81q\x17X\x05\x00\x00\x00SPM25q\x18a}q\x19(h\x08}q\x1aX\x05\x00\x00\x00fastqq\x1bK\x00N\x86q\x1csh\x1bh\x18ubX\x06\x00\x00\x00configq\x1d}q\x1e(X\r\x00\x00\x00brain_samplesq\x1f}q (X\x05\x00\x00\x00SPM13q!X\x05\x00\x00\x00SPM13q"X\x05\x00\x00\x00SPM30q#X\x05\x00\x00\x00SPM30q$X\x05\x00\x00\x00SPM35q%X\x05\x00\x00\x00SPM35q&X\x05\x00\x00\x00SPM28q\'X\x05\x00\x00\x00SPM28q(X\x05\x00\x00\x00SPM15q)X\x05\x00\x00\x00SPM15q*X\x04\x00\x00\x00SPM2q+X\x04\x00\x00\x00SPM2q,X\x05\x00\x00\x00SPM33q-X\x05\x00\x00\x00SPM33q.X\x04\x00\x00\x00SPM8q/X\x04\x00\x00\x00SPM8q0X\x04\x00\x00\x00SPM6q1X\x04\x00\x00\x00SPM6q2X\x05\x00\x00\x00SPM26q3X\x05\x00\x00\x00SPM26q4X\x05\x00\x00\x00SPM25q5X\x05\x00\x00\x00SPM25q6X\x04\x00\x00\x00SPM5q7X\x04\x00\x00\x00SPM5q8X\x05\x00\x00\x00SPM11q9X\x05\x00\x00\x00SPM11q:X\x05\x00\x00\x00SPM36q;X\x05\x00\x00\x00SPM36q<X\x04\x00\x00\x00SPM9q=X\x04\x00\x00\x00SPM9q>X\x05\x00\x00\x00SPM14q?X\x05\x00\x00\x00SPM14q@uX\x11\x00\x00\x00microglia_samplesqA}qB(X\x12\x00\x00\x00S1210100TAGCTTL007qCX\x12\x00\x00\x00S1210100TAGCTTL007qDX\x10\x00\x00\x00S14005GGCTACL007qEX\x10\x00\x00\x00S14005GGCTACL007qFX\x10\x00\x00\x00S12048ATCACGL007qGX\x10\x00\x00\x00S12048ATCACGL007qHX\x10\x00\x00\x00S12082ACTTGAL007qIX\x10\x00\x00\x00S12082ACTTGAL007qJX\x10\x00\x00\x00S12112GATCAGL007qKX\x10\x00\x00\x00S12112GATCAGL007qLX\x10\x00\x00\x00S12067TTAGGCL007qMX\x10\x00\x00\x00S12067TTAGGCL007qNX\x10\x00\x00\x00S15018GTTTCGL007qOX\x10\x00\x00\x00S15018GTTTCGL007qPX\x10\x00\x00\x00S13067GTGGCCL007qQX\x10\x00\x00\x00S13067GTGGCCL007qRuuX\t\x00\x00\x00resourcesqScsnakemake.io\nResources\nqT)\x81qU(K\x01K\x01e}qV(h\x08}qW(X\x06\x00\x00\x00_coresqXK\x00N\x86qYX\x06\x00\x00\x00_nodesqZK\x01N\x86q[uhXK\x01hZK\x01ubX\x07\x00\x00\x00threadsq\\K\x01X\x06\x00\x00\x00outputq]csnakemake.io\nOutputFiles\nq^)\x81q_X\'\x00\x00\x00Data/ScientificNames/Bacteria/SPM25.txtq`a}qah\x08}qbsbX\x06\x00\x00\x00paramsqccsnakemake.io\nParams\nqd)\x81qe}qfh\x08}qgsbub.'); from snakemake.logging import logger; logger.printshellcmds = False
######## Original script #########
#!/usr/bin/env python3

with open(snakemake.output[0], "w") as out:
    AllGenomes = open(snakemake.input[0])
    genomedict = {}
    for line in AllGenomes:
        if line.startswith(">gi"):
            genome = line.split(">")[1].split(",")[0]
            refname = genome.split("| ")[0]
            organism = genome.split("| ")[1]
            print(refname)
            genomedict[refname] = organism

        elif line.startswith(">JPKZ") or line.startswith(">MIEF") or line.startswith(">LL") or line.startswith(">AWXF") or line.startswith("EQ") or line.startswith(">NW_") or line.startswith(">LWMK") or line.startswith(">NZ_") or line.startswith(">NC_") or line.startswith(">KT"):
            genome = line.split(">")[1].split(",")[0]
            refname = genome.split(" ")[0]
            organismName = genome.split(" ")[1:]
            organism = ' '.join(organismName)
            print(refname)
            genomedict[refname] = organism

    accession_nrs = []
    pathogenNames = []

    for line in open(snakemake.input[1]):
        accession_nrs.append(line.split("\t")[0])

    for number in accession_nrs:
        if number in genomedict:
            pathogenNames.append(genomedict[number])

    for i in pathogenNames:
        out.write(number + "\n")
