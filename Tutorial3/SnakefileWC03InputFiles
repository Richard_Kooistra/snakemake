workdir: "/commons/student/2017-2018/Thema11/RKooistra/"
configfile: "config.yaml"

rule all:
    input:
        "out.html"

rule bwa_map:
    input:
        "data/genome.fa",
        "data/samples/{sample}.fastq"
    output:
        "mapped_reads/{sample}.bam"
    message:
        """executing bwa mem on the following \n{input} to generate the following {output}"""
    shell:
        "bwa mem {input} | samtools view -Sb - > {output} "

rule samtools_sort:
    input:
        "mapped_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam"
    message:
        """executing samtools sort on the following {input}"""
    shell:
        "samtools sort -T sorted/reads/{wildcards.sample}" +
        " -O bam {input} > {output}"

rule samtools_index:
    input:
        "sorted_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam.bai"
    message:
        """executing samtools index on the following \n{input}\nto generate the following {output}"""
    shell:
        "samtools index {input}"

rule bcftools_call:
    input:
        genome = "data/genome.fa",
        bam = expand("sorted_reads/{sample}.bam", sample=config["samples"]),
        bai = expand("sorted_reads/{sample}.bam.bai", sample=config["samples"])
    output:
        "calls/all.vcf"
    message:
        """executing variant calling using \n {input.genome}, \n {input.bam}, \n{input.bai} \nto generate {output}"""
    shell:
        "samtools mpileup -g -f {input.genome} {input.bam} | " +
        "bcftools call -mv - > {output}"

rule report:
    input:
        "calls/all.vcf"
    output:
        "out.html"
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeast reference genome
        and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        """, output[0], metadata="Author: Mr Richard Kooistra", T1=input[0])

rule clean:
    shell:
        "rm out.html\n" +
        "rm mapped_reads/*.bam\n" +
        "rm sorted_reads/*.bam\n" +
        "rm sorted_reads/*.bam.bai\n" +
        "rm calls/*.vcf"

