rule bwa_map:
    input:
        "data/genome.fa",
        "data/samples/{sample}.fastq"
    output:
        "mapped_reads/{sample}.bam"
    message:
        """executing bwa mem on the following \n{input} to generate the following {output}"""
    shell:
        "bwa mem {input} | samtools view -Sb - > {output} "
