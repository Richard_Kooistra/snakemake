rule samtools_sort:
    input:
        "mapped_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam"
    message:
        """executing samtools sort on the following {input}"""
    shell:
        "samtools sort -T sorted/reads/{wildcards.sample}" +
        " -O bam {input} > {output}"
