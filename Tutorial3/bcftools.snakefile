rule bcftools_call:
    input:
        genome = "data/genome.fa",
        bam = expand("sorted_reads/{sample}.bam", sample=config["samples"]),
        bai = expand("sorted_reads/{sample}.bam.bai", sample=config["samples"])
    output:
        "calls/all.vcf"
    message:
        """executing variant calling using \n {input.genome}, \n {input.bam}, \n{input.bai} \nto generate {output}"""
    shell:
        "samtools mpileup -g -f {input.genome} {input.bam} | " +
        "bcftools call -mv - > {output}"
