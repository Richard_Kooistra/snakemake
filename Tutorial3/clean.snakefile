rule clean:
    shell:
        "rm out.html\n" +
        "rm mapped_reads/*.bam\n" +
        "rm sorted_reads/*.bam\n" +
        "rm sorted_reads/*.bam.bai\n" +
        "rm calls/*.vcf"
