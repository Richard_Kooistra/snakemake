rule samtools_index:
    input:
        "sorted_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam.bai"
    message:
        """executing samtools index on the following \n{input}\nto generate the following {output}"""
    shell:
        "samtools index {input}"
